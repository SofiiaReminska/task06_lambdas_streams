package com.epam.lab.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ViewStatistics {
    private static final Logger LOGGER = LogManager.getLogger(ViewStatistics.class);

    public static void main(String[] args) {

        IntTripleFunction stat1 = (a, b, c) -> Integer.max(a, Integer.max(b, c));
        IntTripleFunction stat2 = (a, b, c) -> (a + b + c) / 3;

        LOGGER.info("Max value of {-1,3,1} is " + stat1.apply(-1, 3, 1));
        LOGGER.info("Average value of {1,2,3} is " + stat2.apply(1, 2, 3));
    }
}
