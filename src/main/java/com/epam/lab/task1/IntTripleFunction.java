package com.epam.lab.task1;

@FunctionalInterface
public interface IntTripleFunction {
    int apply(int a, int b, int c);
}
