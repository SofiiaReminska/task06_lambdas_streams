package com.epam.lab.task2;

public interface Command {
    public void execute(String value);
}
