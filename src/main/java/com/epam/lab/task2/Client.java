package com.epam.lab.task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Client {
    private static final Logger LOGGER = LogManager.getLogger(Client.class);

    private static final Map<String, Command> MENU_MAP;

    static {
        Map<String, Command> map = new HashMap<>();
        map.put("L", value -> LOGGER.info(String.format("I'm lambda command, I received %s", value)));
        map.put("M", Client::methodReference);
        map.put("A", new Command() {
            @Override
            public void execute(String value) {
                LOGGER.info(String.format("I'm anonymous class command, I received %s", value));
            }
        });
        map.put("O", value -> LOGGER.info(String.format("I'm object of command, I received %s", value)));
        MENU_MAP = Collections.unmodifiableMap(map);
    }

    public static void main(String[] args) {
        final Invoker invoker = new Invoker();
        String message;
        try (Scanner sc = new Scanner(System.in)) {
            LOGGER.info("Choose the command:\nL - lambda command\nM - methodReference\nA - anonymous class\nO - object of command\nQ - exit input command");
            retrieveCommandInput(invoker, sc);
            LOGGER.info("Enter message:");
            message = sc.nextLine();
        }
        invoker.executeCommands(message);
    }

    private static void retrieveCommandInput(Invoker invoker, Scanner sc) {
        String choice = sc.nextLine();
        while (!choice.equalsIgnoreCase("Q")) {
            if (!MENU_MAP.keySet().contains(choice)) {
                LOGGER.info("incorrect input!!!");
                System.exit(0);
            }
            invoker.addCommand(MENU_MAP.get(choice));
            choice = sc.nextLine();
        }
    }

    private static void methodReference(String value) {
        LOGGER.info(String.format("I'm methodReference command, I received %s", value));
    }
}
