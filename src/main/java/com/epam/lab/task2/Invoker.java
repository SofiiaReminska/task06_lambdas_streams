package com.epam.lab.task2;

import java.util.ArrayList;
import java.util.List;

public class Invoker {
    private List<Command> commands;

    public Invoker() {
        commands = new ArrayList<>();
    }

    public void addCommand(Command command) {
        commands.add(command);
    }

    public void executeCommands(String value) {
        commands.forEach((command) -> command.execute(value));
    }
}
