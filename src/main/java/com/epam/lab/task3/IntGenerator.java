package com.epam.lab.task3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class IntGenerator {
    private static final Logger LOGGER = LogManager.getLogger(IntGenerator.class);
    private static final int MAX_NUMBER = 9;
    private static final int MIN_NUMBER = 1;

    public static void generateRandomIntegersAndStatistics(int numberOfGeneratedIntegers) {
        List<Integer> generatedInts = Stream
                .generate(() -> (int) (Math.random() * MAX_NUMBER + MIN_NUMBER))
                .limit(numberOfGeneratedIntegers)
                .collect(Collectors.toList());
        LOGGER.info("Generated list of integers {}", generatedInts);
        final IntSummaryStatistics statistics = generatedInts
                .stream()
                .mapToInt(i -> i)
                .summaryStatistics();
        LOGGER.info(statistics);
        List<Integer> intsBiggerThanAverage = generatedInts
                .stream()
                .mapToInt(i -> i)
                .filter(value -> value > statistics.getAverage())
                .boxed()
                .collect(Collectors.toList());
        LOGGER.info("List of integers more than average {}", intsBiggerThanAverage);
    }

    public static void generateRandomIntegersAndSum(int numberOfGeneratedIntegers) {
        List<Integer> generatedInts = IntStream
                .range(0, numberOfGeneratedIntegers)
                .map((i) -> (int) (Math.random() * MAX_NUMBER + MIN_NUMBER))
                .boxed()
                .collect(Collectors.toList());
        LOGGER.info("Generated list of integers {}", generatedInts);
        Integer sum = generatedInts
                .stream()
                .reduce(0, (a, b) -> a + b);
        LOGGER.info("Sum of integers(reduced): {}", sum);
        Integer sum2 = generatedInts.stream().mapToInt(value -> value).sum();

        LOGGER.info("Sum of integers(sum method): {}", sum2);
    }


    public static void main(String[] args) {
        generateRandomIntegersAndStatistics(10);
        LOGGER.info("----------------------------------------------------------------------");
        generateRandomIntegersAndSum(10);
    }
}
