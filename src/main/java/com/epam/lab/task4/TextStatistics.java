package com.epam.lab.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TextStatistics {
    private static final Logger LOGGER = LogManager.getLogger(TextStatistics.class);
    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        addStringToArray(list);
        LOGGER.info("List of word in text {}", list);
        countNumberDistinctWords(list);
        printSortedListDistinctWords(list);
        countOccurenceNumberWords(list);
        countOccurenceNumberSymbol(list);
    }

    private static void addStringToArray(List<String> list) {
        String line = TextStatistics.sc.nextLine();
        while (!line.equals("")) {
            list.addAll(Arrays.asList(line.split(" ")));
            line = TextStatistics.sc.nextLine();
        }
    }

    private static void countOccurenceNumberSymbol(List<String> list) {
        final Map<Character, Long> occurenceNumberSymbol = list
                .stream()
                .flatMap(s -> s.chars().boxed()).map(i -> (char) i.intValue())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        LOGGER.info("Occurence number of each symbol in text {}", occurenceNumberSymbol);
    }

    private static void countOccurenceNumberWords(List<String> list) {
        Map<String, Long> occurenceNumberOfWord = list
                .stream()
                .sorted()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        LOGGER.info("Occurence number of each word in text {}", occurenceNumberOfWord);
    }

    private static void printSortedListDistinctWords(List<String> list) {
        final List<String> listOfDistinctWords = list
                .stream()
                .map(String::toLowerCase)
                .distinct().sorted()
                .collect(Collectors.toList());
        LOGGER.info("Sorted list of distinct words {}", listOfDistinctWords);
    }

    private static void countNumberDistinctWords(List<String> list) {
        final long numberOfDistinctWords = list
                .stream()
                .map(String::toLowerCase)
                .distinct().count();
        LOGGER.info("Number of distinct words in text {}", numberOfDistinctWords);
    }
}
